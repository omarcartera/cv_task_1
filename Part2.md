# Part 2: Written

You answers to this part should be included in the `Part2.md` file in the repository of your assignment.


Also, your answers should be formatted as *Markdown* syntax.

## Q1

Transform a few more (easy) RGB values manually into corresponding HSI values.

### Answer

*Your answer here*

## Q2

In the CIE’s RGB colour space (which models human colour perception), the scalars R, G, or B may also be negative. Provide a physical interpretation (obviously, we cannot subtract light from a given spectrum).

### Answer

*Your answer here*