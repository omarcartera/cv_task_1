# import the necessary packages
from os import listdir
from os.path import isfile , join
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np


images_files = [ join("./images" , f) for f in listdir("images") if isfile(join("images" , f)) ]
RGB_images = [ mpimg.imread( f ) for f in images_files ]

count = len(RGB_images)

fig = plt.figure(figsize=(10,20))


def view_img(row, title):
	fig.add_subplot(count, 4, (row*4)+1)
	plt.imshow(RGB_images[row-1])

	if(row == 0):
		plt.title(title)

	plt.xticks([])
	plt.yticks([])

def view_hist(row, title, page):
	fig.add_subplot(count, 4, (row*4)+(page+2))
	plt.hist(RGB_images[row-1][page], 256)

	if(row == 0):
		plt.title(title)
	
	plt.xticks([])
	plt.yticks([])


def plot_multi(images):
	for row in range(count):
		view_img(row, 'Image')

		view_hist(row, 'Red', 0)

		view_hist(row, 'Green', 1)

		view_hist(row, 'Blue', 2)


	plt.show()


plot_multi(RGB_images)